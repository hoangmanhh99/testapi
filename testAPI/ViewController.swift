//
//  ViewController.swift
//  testAPI
//
//  Created by MacOS on 17/03/2021.
//

import UIKit
import Moya

class ViewController: UIViewController {
    @IBOutlet weak var newsTableView: UITableView!
    @IBOutlet weak var viewMessage: UIView!
    @IBOutlet weak var imgMessage: UIImageView!
    @IBOutlet weak var lblMessage: UILabel!
    
    // MARK: - View State
    private var state: State = .loading {
        didSet {
            switch state {
            case .ready:
                viewMessage.isHidden = true
                newsTableView.isHidden = false
                newsTableView.reloadData()
            case .loading:
                newsTableView.isHidden = true
                viewMessage.isHidden = false
                lblMessage.text = "Getting comics ..."
                imgMessage.image = #imageLiteral(resourceName: "Loading")
            case .error:
                newsTableView.isHidden = true
                viewMessage.isHidden = false
                lblMessage.text = """
                              Something went wrong!
                              Try again later.
                            """
                imgMessage.image = #imageLiteral(resourceName: "Error")
            }
        }
    }
    
    let provider = MoyaProvider<Marvel>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let nib = UINib(nibName: "NewsTableViewCell", bundle: .main)
        newsTableView.register(nib, forCellReuseIdentifier: reuseIdentifier)
        
        state = .loading
        provider.request(.comics) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                do {
                    self.state = .ready(try response.map(MarvelResponse<Comic>.self).data.results)
                } catch {
                    self.state = .error
                }
            case .failure:
                self.state = .error
            }
        }
    }
}

extension ViewController {
    enum State {
        case loading
        case ready([Comic])
        case error
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard case .ready(let items) = state else { return 0 }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = newsTableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? NewsTableViewCell else { return UITableViewCell() }
        guard case .ready(let items) = state else { return cell }
        
        cell.configureWith(items[indexPath.item])
        
        return cell
    }
    
}
