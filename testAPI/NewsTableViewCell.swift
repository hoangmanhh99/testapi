//
//  NewsTableViewCell.swift
//  testAPI
//
//  Created by MacOS on 17/03/2021.
//

import UIKit
import Kingfisher

let reuseIdentifier = "cell"

class NewsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var thumbImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func configureWith(_ comic: Comic) {
      titleLabel.text = comic.title
      contentLabel.text = comic.description ?? "No description available"

      thumbImageView.kf.setImage(with: comic.thumbnail.url,
                           options: [.transition(.fade(0.3))])
    }
    
}
