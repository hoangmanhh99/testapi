//
//  Marvel.swift
//  testAPI
//
//  Created by MacOS on 17/03/2021.
//

import Foundation
import Moya

public enum Marvel {
    
    static private let publicKey = "33a1e402c2cbd22e72d9135c2fb07f50"
    static private let privateKey = "37ecae54d22f85ebd5083d36db77e9a177f12b7a"
    
    case comics
}

extension Marvel: TargetType {
    public var baseURL: URL {
        return URL(string: "https://gateway.marvel.com/v1/public")!
    }
    
    public var path: String {
        switch self {
        case .comics:
            return "/comics"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .comics:
            return .get
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        let ts = "\(Date().timeIntervalSince1970)"
        
        let hash = (ts + Marvel.privateKey + Marvel.publicKey).md5
        
        let authParams = ["apikey": Marvel.publicKey, "ts": ts, "hash": hash]
        
        switch self {
        case .comics:
            return .requestParameters(
                parameters: [
                    "format": "comic",
                    "formatType": "comic",
                    "orderBy": "-onsaleDate",
                    "dateDescriptor": "lastWeek",
                    "limit": 50] + authParams, encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return ["Content-Type":"application/json"]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}
